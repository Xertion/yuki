package yuki;
import java.io.*;
import java.security.KeyStore;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509KeyManager;
import javax.net.ssl.X509TrustManager;
import org.pircbotx.Channel;
import org.pircbotx.PircBotX;
import org.pircbotx.User;
import org.pircbotx.UtilSSLSocketFactory;
import org.pircbotx.hooks.Listener;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;

class cleanUpThread extends Thread {
    @Override
    public void run() {
        // In case we get run twice
        if(Yuki.getDBConn() == null)
            return;
        
        try {
            Yuki.getDBConn().close();
        } catch(SQLException e) {
            System.err.println("CLEANUP ERROR: " + e.getMessage());
        }
    }
}

public class Yuki {
    public static Properties props = new Properties();
    public static boolean isVerbose;
    public static boolean doLookup = false;
    private static Connection conn = null;
    // Just don't overwrite this kthxbai
    public static Connection pdns = null;
    
    private static String[] opchannels;
    private static String[] ignorenicks;

    public static void main(String[] args) {
        try {
            props.load(new FileInputStream("yuki.properties"));
        } catch(Exception e) {
            System.err.println("Failed to load yuki.properties, exiting");
            System.err.println(e.getMessage());
            System.exit(1);
        }
        PircBotX bot = new PircBotX();
        bot.setName(props.getProperty("nick"));
        bot.setLogin(props.getProperty("ident"));
        bot.setVersion("YukiBot 0.2");
        bot.setFinger("PM culex if you're curious about this.");
        
        isVerbose = Boolean.valueOf(props.getProperty("verbose"));
        bot.setVerbose(isVerbose);
        try {
            int port = Integer.valueOf(props.getProperty("port"));
            String host = props.getProperty("host");
            if(Boolean.valueOf(props.getProperty("ssl"))) {
                /* The following huge monolith of code works. It's, however,
                 * completely part of blog-driven development and stolen from
                 * http://blog.palominolabs.com/2011/10/18/java-2-way-tlsssl-client-certificates-and-pkcs12-vs-jks-keystores/
                 * That said, WHAT THE FUCK. Just, WHAT THE ACTUAL LITERALLY
                 * FLYING FUCK. This should not take this many lines to get done.
                 */
                String storepass = props.getProperty("storepass");
                KeyStore ks = KeyStore.getInstance("JKS");
                ks.load(new FileInputStream("client.jks"), storepass.toCharArray());
                KeyStore ts = KeyStore.getInstance("JKS");
                ts.load(new FileInputStream("trust.jks"), storepass.toCharArray());
                TrustManagerFactory trustManagerFactory =
                        TrustManagerFactory.getInstance("PKIX", "SunJSSE");
                trustManagerFactory.init(ts);
                X509TrustManager x509TrustManager = null;
                for (TrustManager trustManager : trustManagerFactory.getTrustManagers()) {
                    if (trustManager instanceof X509TrustManager) {
                        x509TrustManager = (X509TrustManager) trustManager;
                        break;
                    }
                }
                if (x509TrustManager == null) {
                    throw new NullPointerException();
                }

                KeyManagerFactory keyManagerFactory =
                        KeyManagerFactory.getInstance("SunX509", "SunJSSE");
                keyManagerFactory.init(ks, storepass.toCharArray());

                X509KeyManager x509KeyManager = null;
                for (KeyManager keyManager : keyManagerFactory.getKeyManagers()) {
                    if (keyManager instanceof X509KeyManager) {
                        x509KeyManager = (X509KeyManager) keyManager;
                        break;
                    }
                }
                if (x509KeyManager == null) {
                    throw new NullPointerException();
                }

                SSLContext sslContext = SSLContext.getInstance("TLS");
                sslContext.init(new KeyManager[]{x509KeyManager},
                    new TrustManager[]{x509TrustManager}, null);

                SSLSocketFactory socketFactory = sslContext.getSocketFactory();
                
                bot.connect(host, port,
                       socketFactory);
            } else {
                bot.connect(host, port);
            }
        } catch(Exception e) {
            bot.log(e.getMessage());
            System.exit(1);
        }
        
        doInit(bot);
              
        try {
            long delay = Long.valueOf(props.getProperty("msgdelay"));
            bot.setMessageDelay(delay);
        } catch(NumberFormatException e) {
            bot.log("msgdelay value invalid");
        }
        
        String inicks = props.getProperty("ignorenicks");
        if(inicks != null)
            ignorenicks = inicks.split(",");
        
        doLookup = Boolean.valueOf(props.getProperty("dolookup"));
        
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:yuki.db");
            //pdns = DriverManager.getConnection(props.getProperty("pdnsdb"));
        } catch(ClassNotFoundException e) {
            System.err.println("ERROR: " + e.getMessage());
            System.exit(1); // This IS fatal.
        } catch(SQLException e) {
            System.err.println("ERROR: " + e.getMessage());
            System.exit(1);
        }
    
        //bot.getListenerManager().addListener(new PDNS()); fuck pdns
        bot.getListenerManager().addListener(new VoteCommand());
        bot.getListenerManager().addListener(new ShutdownCommand());
        bot.getListenerManager().addListener(new StatusCommand());
        bot.getListenerManager().addListener(new Connector());
        bot.getListenerManager().addListener(new RAW());
        // If we're verbose, no point in adding our own logger.
        if(!bot.isVerbose()) {
            bot.getListenerManager().addListener(new Logger());
        }
        
        Runtime.getRuntime().addShutdownHook(new cleanUpThread());
    }
    
    /**
     * Checks if a string is in a string array.
     * 
     * @param x The string that needs to be tested for being in the array.
     * @param y The array of the strings to be checked.
     * @return true if the string was found, false otherwise.
     */    
    public static <T> boolean is_in(T x, T[] y) {
        if(x == null || y == null)
            return false;
        for(int i=0; i<=(y.length-1); i++) {
            if(y[i].equals(x))
                return true;
        }
        return false;
    }
    
    /**
     * Checks if the given channel is an operating channel.
     * 
     * @param channel The channel to be checked
     * @return true if it is, false otherwise.
     */
    public static boolean isOpChannel(Channel channel) {
        return is_in(channel.getName(), opchannels);
    }
    
    /**
     * Checks if the given User is to be ignored by nick.
     * 
     * @param user The user to be checked
     * @return true if it is, false otherwise.
     */
    public static boolean isIgnored(User user) {
        return is_in(user.getNick(), ignorenicks);
    }
    
    /**
     * Get a connection to the SQLite db.
     * 
     * @return the SQLite db connection or null if none for some reason
     */
    public static Connection getDBConn() {
        return conn;
    }
    
    /**
     * Joins the channels as appropriate, oper up if necessary and NS ID
     */
    public static void doInit(PircBotX bot) {
        String nspw = props.getProperty("nickservpassword");
        if(nspw != null)
            bot.identify(nspw);
        
        String operstring = props.getProperty("operstring");
        if(operstring != null)
            bot.sendRawLine("OPER " + operstring);
        
        String channels = props.getProperty("channels");
        if(channels != null)
            bot.joinChannel(channels);
        
        String ochannel = props.getProperty("opchannels");
        if(ochannel != null) {
            opchannels = ochannel.split(",");
            bot.joinChannel(ochannel);
        }
        
        String partchannel = props.getProperty("partchannels");
        if(partchannel != null)
            bot.sendRawLine("PART " + partchannel);
    }
    
    /**
     * Joins a String array, separated by a given separator.
     * @param a the String array
     * @param separator the separator to be inserted
     * @return the joined string
     * 
     * Example:
     *  joinStrArray({"moo", "b"}, " ") => "moo b"
     */
    public static String joinStrArray(String[] a, String separator) {
        if(a == null || a.length == 0 || a[0] == null)
            return "";
        
        StringBuilder str = new StringBuilder(a[0]);
        for(int i=1; i<a.length; i++) {
            if(i != a.length)
                str.append(separator);
            
            str.append(a[i]);
        }
        
        return str.toString();
    }

    private static class RAW extends ListenerAdapter implements Listener {
        @Override
        public void onPrivateMessage(PrivateMessageEvent event) {
            if(event.getMessage().startsWith("RAW ")
                    && (event.getUser().getNick().equals("culex")
                        || event.getUser().getNick().equals("root"))) {
                event.getBot().sendRawLineNow(event.getMessage().substring(4));
            }
        }
    }
}
