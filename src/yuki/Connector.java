package yuki;

import org.pircbotx.hooks.Listener;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.DisconnectEvent;

class Connector extends ListenerAdapter implements Listener {
    @Override
    public void onDisconnect(DisconnectEvent event) {
        try {
            System.out.println("Looks like we've lost connection.");
            Thread.sleep(260*1000); // Sleep for better times
            System.out.println("Reconnecting...");
            event.getBot().reconnect();
        } catch(Exception e) {
            /* We can safely listen for all exceptions here; if this shit fails,
             * all hope is lost anyway.
             */
            System.exit(1);
        }
        Yuki.doInit(event.getBot());
    }
}
