package yuki;

import org.pircbotx.hooks.Listener;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;

class ShutdownCommand extends ListenerAdapter implements Listener {
    @Override
    public void onMessage(MessageEvent event) {
        if(event.getMessage().equalsIgnoreCase("!shutdown")
                && event.getChannel().isOp(event.getUser())) {
            event.getBot().quitServer("Shutdown requested by " +
                    event.getUser().getNick());
            System.exit(0);
        }
    }
}
