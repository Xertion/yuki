package yuki;

import org.pircbotx.hooks.Listener;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.*;

class Logger extends ListenerAdapter implements Listener {
    private void alog(String line) {
        System.out.println(System.currentTimeMillis() + " " + line);
    }
    
    @Override
    public void onMessage(MessageEvent event) {
        alog("<" + event.getUser().getNick() + ":"
                + event.getChannel().getName() + "> " + event.getMessage());
    }
    
    @Override
    public void onAction(ActionEvent event) {
        alog("<" + event.getUser().getNick() + ":"
                + ((event.getChannel() == null) ? event.getChannel().getName() : "") + "> " + event.getMessage());
    }
    
    @Override
    public void onPrivateMessage(PrivateMessageEvent event) {
        alog("<" + event.getUser().getNick() + "> " + event.getMessage());
    }
    
    @Override
    public void onJoin(JoinEvent event) {
        alog("*** " + event.getUser().getNick() + " joined "
                + event.getChannel().getName());
    }
    
    @Override
    public void onKick(KickEvent event) {
        alog("*** " + event.getSource().getNick() + " kicked "
                + event.getRecipient().getNick() + " from "
                + event.getChannel() + ": " + event.getReason());
    }
    
    @Override
    public void onMode(ModeEvent event) {
        alog("*** " + event.getUser().getNick() + " changed modes on "
                + event.getChannel().getName() + ": " + event.getMode());
    }
    
    @Override
    public void onNickChange(NickChangeEvent event) {
        alog("*** " + event.getOldNick() + " is now known as "
                + event.getNewNick());
    }
    
    @Override
    public void onNotice(NoticeEvent event) {
        String add = "";
        if(event.getChannel() == null)
            add = "[" + event.getChannel() + "]";
        alog("-" + event.getUser().getNick() + "-" + add + event.getMessage());
    }
    
    @Override
    public void onPart(PartEvent event) {
        alog("*** " + event.getUser().getNick() + " has left "
                + event.getChannel().getName() + ": " + event.getReason());
    }
    
    @Override
    public void onQuit(QuitEvent event) {
        alog("*** " + event.getUser().getNick() + " has quit: "
                + event.getReason());
    }
    
    @Override
    public void onTopic(TopicEvent event) {
        alog("*** " + event.getUser().getNick() + " has changed the topic of "
                + event.getChannel().getName() + " to: " + event.getTopic());
    }
    
    @Override
    public void onUserMode(UserModeEvent event) {
        alog("*** User mode change: " + event.getMode());
    }
}
