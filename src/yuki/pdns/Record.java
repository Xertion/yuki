package yuki.pdns;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.joda.time.DateTime;
import yuki.Yuki;

/**
 * Specifies a DNS record.
 */
public class Record {
    protected static final int domain_id;
    protected static final int ttl = 1800;
    static {
        domain_id = Integer.valueOf(Yuki.props.getProperty("domain_id"));
    }
    
    protected String name;
    protected String type;
    protected String content;
    protected boolean frozen = false;
    // If null, do not expire
    protected DateTime freezexpiry = null;
    
    /**
     * Creates a new DNS record. This doesn't write back to db.
     * @param name domain name (excluding .xertion.org)
     * @param type domain type (AAAA, CNAME, ...)
     * @param content content of the entry
     */
    protected Record(String name, String type, String content) {
        this.name = name.toLowerCase();
        this.type = type.toUpperCase();
        this.content = content;
    }
    
    /**
     * Writes this object to the database.
     * @throws SQLException if the underlying MySQL db fails for some reason
     */
    public void save() throws SQLException {
        PreparedStatement s;
        
        deleteFromDatabase();
        
        if(frozen)
            return;
        
        s = Yuki.pdns.prepareStatement("INSERT INTO `records`(domain_id, name, type, content, ttl, prio, change_date) VALUES(?,?,?,?,?,?,?)");
        s.setInt(1, domain_id);
        s.setString(2, this.getName());
        s.setString(3, type);
        s.setString(4, content);
        s.setInt(5, ttl);
        s.setInt(6, 0);
        s.setInt(7, (int)(System.currentTimeMillis()/1000));
        s.executeUpdate();
    }
    
    /**
     * Deletes this object from the database, at least temporarily.
     * @throws SQLException if the underlying MySQL db fails for some reason
     */
    public void deleteFromDatabase() throws SQLException {
        PreparedStatement s;
                
        s = Yuki.pdns.prepareStatement("DELETE FROM `records` WHERE name = ? AND type = ? AND content = ?");
        s.setString(1, this.getName());
        s.setString(2, type);
        s.setString(3, content);
        s.executeUpdate();
    }
    
    /**
     * @return the name for this record, including the trailing .xertion.org
     */
    public String getName() {
        return name + ".xertion.org";
    }
    
    /**
     * @return the name for this record, excluding the trailing .xertion.org
     */
    public String getBaseName() {
        return name;
    }
    
    /**
     * @return the type of the record (A, AAAA, CNAME, ...)
     */
    public String getType() {
        return type;
    }
    
    /**
     * @return the content of the record
     */
    public String getContent() {
        return content;
    }
    
    /**
     * (Un-)Freezes this record. This will automatically save the record, too.
     * @param frozen if true, freeze. If false, unfreeze
     * @param freezexpiry the DateTime when this freeze shall expire (or null if
     *                    it's not supposed to expire at all)
     */
    public void setFrozen(boolean frozen, DateTime freezexpiry) {
        this.frozen = frozen;
        this.freezexpiry = freezexpiry;
        
        try {
            save();
        } catch(SQLException e) {
            System.err.println("Error while freezing server: " + e.getMessage());
        }
    }
    
    @Override
    public String toString() {
        return this.getName() + " (" + this.type + ", " + this.content + ")";
    }
}
