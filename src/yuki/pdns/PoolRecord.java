package yuki.pdns;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.joda.time.DateTime;
import yuki.Yuki;

/**
 * Describes a pool record.
 */
public final class PoolRecord extends Record {
    private enum PoolAction {
        REGION, POOL;
    }
    
    private String region;
    
    /**
     * Creates a new DNS record. This doesn't write back to db.
     * @param name domain name (including .xertion.org)
     * @param type domain type (AAAA, CNAME, ...)
     * @param content content of the entry
     */
    protected PoolRecord(String name, String type, String content, String region) {
        super(name, type, content);
        this.region = region.toLowerCase();
    }
    
    /**
     * Writes this object to the database; its actual name, for the pool(6) and
     * the regional pool.
     * @throws SQLException if the underlying MySQL db fails for some reason
     */
    @Override
    public void save() throws SQLException {
        PreparedStatement s;
        
        /*
         * We do two iterations. One is for adding the record to
         *.iso.xertion.org, the other is to add it to the general pool(6).
         */
        
        // WHERE TO GO WITH YOU?
        super.save(); // also calls our deleteFromDatabase
        
        for(PoolAction pa : PoolAction.values()) {
            String myname;
            
            switch(pa) {
                case REGION:
                    myname = region + ".iso.xertion.org";
                    break;
                case POOL:
                    myname = "pool" + (type.equals("A") ? "" : "6") + ".xertion.org";
                    break;
                default:
                    System.err.println("Unknown PoolAction " + pa.name());
                    continue; // This should never happen
            }
            
            if(frozen)
                continue;
            
            s = Yuki.pdns.prepareStatement("INSERT INTO `records`(domain_id, name, type, content, ttl, prio, change_date) VALUES(?,?,?,?,?,?,?)");
            s.setInt(1, domain_id);
            s.setString(2, myname);
            s.setString(3, type);
            s.setString(4, content);
            s.setInt(5, Record.ttl);
            s.setInt(6, 0);
            s.setInt(7, (int)(System.currentTimeMillis()/1000));
            s.executeUpdate();
        }
    }
    
    /**
     * Deletes this object from the database, at least temporarily.
     * @throws SQLException if the underlying MySQL db fails for some reason
     */
    @Override
    public void deleteFromDatabase() throws SQLException {
        PreparedStatement s;
        
        super.deleteFromDatabase();
        
        for(PoolAction pa : PoolAction.values()) {
            String myname;
            
            switch(pa) {
                case REGION:
                    myname = region + ".iso.xertion.org";
                    break;
                case POOL:
                    myname = "pool" + (type.equals("A") ? "" : "6") + ".xertion.org";
                    break;
                default:
                    System.err.println("Unknown PoolAction " + pa.name());
                    continue; // This should never happen
            }
            s = Yuki.pdns.prepareStatement("DELETE FROM `records` WHERE name = ? AND type = ? AND content = ?");
            s.setString(1, myname);
            s.setString(2, type);
            s.setString(3, content);
            s.executeUpdate();
        }
    }
    
    /**
     * @return the ISO region abbreviation this entry belongs to
     */
    public String getRegion() {
        return region;
    }
    
    /**
     * (Un-)Freezes this record. This will automatically save the record, too.
     * @param frozen if true, freeze. If false, unfreeze
     * @param freezexpiry the DateTime when this freeze shall expire (or null if
     *                    it's not supposed to expire at all)
     */
    @Override
    public void setFrozen(boolean frozen, DateTime freezexpiry) {
        super.setFrozen(frozen, freezexpiry);
    }
}
