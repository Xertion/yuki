package yuki.pdns;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import yuki.Yuki;

/**
 * Creates Records. This class SHALL NOT be instated.
 */
public class RecordFactory {
    private static final Set<Record> records = Collections.synchronizedSet(new HashSet<Record>());
    private static final String[] regions = 
       {"af",  // Africa
        "as",  // Asia
        "eu",  // Europe
        "na",  // North America
        "sa",  // South America
        "oc",  // Oceania
        "an"}; // Antarctica
    
    static {
        /* We should read the database's contents in for starters.
         * 
         * Ideally, we start with the geo records (i.e. regions.iso.xertion.org)
         * because those contain IPs we can use for identifying later records as
         * PoolRecords rather than regular Records.
         */
        PreparedStatement s;
        ResultSet rs;
        Record r;
        
        try {
            s = Yuki.pdns.prepareStatement(
                    "SELECT r1.name AS poolname, r1.type AS type, r1.content AS ip, r2.name AS realname FROM records"
                    + " AS r1 LEFT OUTER JOIN records r2 ON (r1.content = r2.content)"
                    + " WHERE r1.name LIKE '%.iso.xertion.org' AND r2.name NOT LIKE 'pool%' AND r2.name NOT LIKE '%.iso.xertion.org'");
            rs = s.executeQuery();
            
            while(rs.next()) {
                String realname = rs.getString("realname");
                r = new PoolRecord(realname.substring(0, realname.indexOf('.')),
                        rs.getString("type"),
                        rs.getString("ip"),
                        rs.getString("poolname"));
                
                records.add(r);
            }
            
            s = Yuki.pdns.prepareStatement("SELECT name, type, content FROM records WHERE name NOT LIKE 'pool%' AND name NOT LIKE '%.iso.xertion.org'");
            rs = s.executeQuery();
            
            while(rs.next()) {
                String realname = rs.getString("name");
                String type = rs.getString("type");
                checkValidEntry(realname, type);
                r = new Record(realname.substring(0, realname.indexOf('.')),
                        rs.getString("type"),
                        rs.getString("content"));
                
                records.add(r);
            }
        } catch(SQLException ex) {
            System.err.println("SQLException while reading records: "
                    + ex.getMessage());
        } catch(RecordValidityException ex) {
            // This is supposed to happen for, e.g., MX.
        }
    }
    
    private RecordFactory() {
        throw new AssertionError();
    }
    
    /**
     * Checks if the given name is a valid name before .xertion.org. The
     * exception exists for the sole purpose of custom messages. If this
     * method runs through without throwing an exception, the entry is valid.
     * @param name the name to be checked
     * @throws RecordValidityException if the record was invalid
     */
    private static void checkValidEntry(String name, String type)
            throws RecordValidityException {
        if(name.toLowerCase().contains("xertion")) {
            throw new RecordValidityException("name MUST NOT contain the trailing .xertion.org!");
        } else if(name.endsWith(".")) {
            throw new RecordValidityException("name MUST NOT end in a dot!");
        } else if(!(type.equals("AAAA")
                || type.equals("A")
                || type.equals("CNAME")
                // || type.equals("MX") We can't MX because no priority interface from !dns
                || type.equals("NS")
                || type.equals("TXT")
                || type.equals("SRV"))) {
            throw new RecordValidityException(type + " is not a valid record type (E.g. A or CNAME).");
        }
    }
    
    /**
     * Creates a new static record. These records are unaffected by the pdns
     * geo backend. This writes the Record and adds it to the internal list of
     * Records.
     * @param name name of the domain to be added. Will be sanity checked here
     * @param type the type of the domain (A, AAAA, CNAME, ...)
     * @param content what to set the domain to
     * @throws RecordValidityException if the given name is invalid
     * @throws SQLException if the Record couldn't be written
     * @return the Record that was created
     */
    public static Record createStaticRecord(String name, String type, String content)
            throws RecordValidityException, SQLException {
        Record r;
        
        checkValidEntry(name, type);
         
        r = new Record(name, type, content);
        r.save();
        records.add(r);
        return r;
    }
    
    /**
     * Creates a new pool record. This will add them to the regional pool
     * (region.iso.xertion.org), the general pool (pool.xertion.org) and create
     * their own DNS by name.xertion.org, too.
     * @param name name of the domain to be added. Gets sanity checked here
     * @param region short region code (NA, EU, ...)
     * @param ip IP(v4) address. Sanity checked.
     * @return the created PoolRecord for name.xertion.org
     * @throws RecordValidityException if the given data was invalid
     * @throws SQLException if the underlying SQL db failed
     */
    public static PoolRecord createPoolRecord(String name, String region, String ip)
            throws RecordValidityException, SQLException {
        PoolRecord r;
        String type;
        
        if(!Yuki.is_in(region.toLowerCase(), regions)) {
            throw new RecordValidityException(region + " is not a valid region (E.g., NA, EU, OC)");
        }
        
        ip = ip.toLowerCase();
        if(ip.matches("\\A(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}\\z")) {
            type = "A";
        } else if(ip.matches("^(((?=(?>.*?::)(?!.*::)))(::)?([0-9a-f]{1,4}::?){0,5}|([0-9a-f]{1,4}:){6})(\\2([0-9a-f]{1,4}(::?|$)){0,2}|((25[0-5]|(2[0-4]|1\\d|[1-9])?\\d)(\\.|$)){4}|[0-9a-f]{1,4}:[0-9a-f]{1,4})(?<![^:]:|\\.)\\z")) {
            type = "AAAA";
        } else {
            throw new RecordValidityException(ip + " is neither a valid IPv4 nor a valid IPv6 IP.");
        }
        
        checkValidEntry(name, type);
        
        r = new PoolRecord(name, type, ip, region);
        r.save();
        records.add(r);
        
        return r;
    }
    
    /**
     * Returns an ArrayList<Record> of records with the given name.
     * @param name the name to be found
     * @return an ArrayList with Records which may be empty.
     */
    public static ArrayList<Record> getRecordsByName(String name) {
        ArrayList<Record> al = new ArrayList<Record>();
        for(Record r : records) {
            if(r.getName().contains(name))
                al.add(r);
        }
        
        return al;
    }
}
