package yuki.pdns;

/**
 * An exception that is thrown for creation if invalid records.
 */
public class RecordValidityException extends Exception {
    public RecordValidityException(String message) {
        super(message);
    }
}
