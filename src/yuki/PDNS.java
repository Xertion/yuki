package yuki;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import org.pircbotx.Channel;
import org.pircbotx.PircBotX;
import org.pircbotx.User;
import org.pircbotx.hooks.Listener;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import yuki.pdns.PoolRecord;
import yuki.pdns.Record;
import yuki.pdns.RecordFactory;
import yuki.pdns.RecordValidityException;

/**
 * Handles management of PowerDNS.
 * 
 * PowerDNS uses two relevant MySQL tables in the current setup:
 * create table domains (
 *  id		 INT auto_increment,
 *  name	 VARCHAR(255) NOT NULL,
 *  master	 VARCHAR(128) DEFAULT NULL,
 *  last_check	 INT DEFAULT NULL,
 *  type	 VARCHAR(6) NOT NULL,
 *  notified_serial INT DEFAULT NULL, 
 *  account      VARCHAR(40) DEFAULT NULL,
 *  primary key (id)
 * ) Engine=InnoDB;

 * CREATE TABLE records (
 *  id              INT auto_increment,
 *  domain_id       INT DEFAULT NULL,
 *  name            VARCHAR(255) DEFAULT NULL,
 *  type            VARCHAR(10) DEFAULT NULL,
 *  content         VARCHAR(64000) DEFAULT NULL,
 *  ttl             INT DEFAULT NULL,
 *  prio            INT DEFAULT NULL,
 *  change_date     INT DEFAULT NULL,
 *  primary key(id)
 * ) Engine=InnoDB;
 * 
 * We need to know the id of domains for xertion.org; which we're reading from
 * properties.
 */
public class PDNS extends ListenerAdapter implements Listener {
    
    @Override
    public void onMessage(MessageEvent e) {
        User u = e.getUser();
        Channel c = e.getChannel();
        PircBotX bot = e.getBot();
        if(!Yuki.isOpChannel(c)
           || Yuki.isIgnored(u)
           || !e.getMessage().startsWith("!dns"))
            return;
        
        String[] params = e.getMessage().split(" ");
        if(params.length < 2) {
            bot.sendNotice(u, "!dns add-static <name> <type> <content> / name MUST NOT contain the trailing .xertion.org");
            bot.sendNotice(u, "!dns add <name> <region code> <ip> / name MUST NOT contain the trailing .xertion.org; adds to pool and region");
            bot.sendNotice(u, "!dns freeze <name> [time in 1d3h2m1s...] / temporarily remove a server until unfrozen or expired");
            bot.sendNotice(u, "!dns unfreeze <name> / add server back to RR");
            bot.sendNotice(u, "!dns del <name> [type] / removes any entries matching <name> (with type [type]) PERMANENTLY");
            bot.sendNotice(u, "!dns list / shows list of DNS entries");
            return;
        }
        
        String[] args = Arrays.copyOfRange(params, 1, params.length);
        String cmd = args[0].toUpperCase();
        PreparedStatement s;
        ResultSet rs;
        
        try {
            if(cmd.equals("ADD-STATIC")) {
                if(args.length < 4) {
                    bot.sendNotice(u, "Syntax: !dns add-static <name> <type> <content> / name MUST NOT contain the trailing .xertion.org");
                    return;
                }
                
                Record r;
                try {
                    r = RecordFactory.createStaticRecord(args[1], args[2],
                            Yuki.joinStrArray(
                                Arrays.copyOfRange(args, 3, args.length), " "));
                } catch(RecordValidityException ex) {
                    bot.sendNotice(c, "Invalid record: " + e.getMessage());
                    return;
                }
                
                bot.sendNotice(c, "Set " + r.getName() + " (" + r.getType() + ") to " + r.getContent() + ".");
            } else if(cmd.equals("ADD")) {
                if(args.length < 4) {
                    bot.sendNotice(u, "Syntax: !dns add <name> <region code> <ip> / name MUST NOT contain the trailing .xertion.org");
                    return;
                }
                
                PoolRecord r;
                try {
                    r = RecordFactory.createPoolRecord(args[1], args[2], args[3]);
                } catch(RecordValidityException ex) {
                    bot.sendNotice(c, "Invalid record: " + ex.getMessage());
                    return;
                }
                
                bot.sendNotice(c, "Added " + r.getName() + " to the RR for "
                        + r.getRegion().toUpperCase() + ".");
            } else if(cmd.equals("DEL")) {
                String type = null;
                boolean deleted = false;
                
                if(args.length < 2) {
                    bot.sendNotice(u, "Syntax: !dns del <name> [type] / removes any entries matching <name> (with type [type]) PERMANENTLY");
                    return;
                }
                if(args.length >= 3)
                    type = args[2];
                
                for(Record r : RecordFactory.getRecordsByName(args[1])) {
                    if(type != null) {
                        if(type.equals(r.getType())) {
                            r.deleteFromDatabase();
                            bot.sendNotice(u, "Deleted " + r.toString() + ".");
                            deleted = true;
                        }
                    } else {
                        r.deleteFromDatabase();
                        bot.sendNotice(u, "Deleted " + r.toString() + ".");
                        deleted = true;
                    }
                }
                
                if(!deleted)
                    bot.sendNotice(u, "No records matched; nothing deleted.");
            }
        } catch(SQLException ex) {
            System.err.println(ex.getMessage());
            bot.sendNotice(u, "A MySQL error has occurred.");
        }
    }
}
