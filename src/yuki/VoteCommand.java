package yuki;

import org.pircbotx.PircBotX;
import org.pircbotx.Channel;
import org.pircbotx.User;
import org.pircbotx.hooks.Listener;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.NoticeEvent;
import org.pircbotx.hooks.events.OpEvent;

import java.util.HashSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;

class VoteCommand extends ListenerAdapter implements Listener {
    // "root" => "culex"
    private final HashMap<String,String> groupnick = new HashMap<String,String>();
    private Connection conn = null;
    
    public VoteCommand() {
        try {
            conn = Yuki.getDBConn();
            if(conn == null) {
                System.err.println("ERROR: No db connection");
                System.exit(1);
            }
            conn.createStatement().executeUpdate(
                    "CREATE TABLE IF NOT EXISTS vote_results ("
                    + "vote    INTEGER,"
                    + "voter   TEXT,"
                    + "voteid  INTEGER,"
                    + "FOREIGN KEY(voteid) REFERENCES votes(id)"
                    + ")");
            conn.createStatement().executeUpdate(
                    "CREATE TABLE IF NOT EXISTS votes ("
                    + "id      INTEGER PRIMARY KEY,"
                    + "title   TEXT,"
                    + "owner   TEXT,"
                    + "channel TEXT,"
                    + "date    INTEGER,"
                    + "closed  INTEGER DEFAULT 0" // New polls are always open
                    + ")");
        } catch(SQLException e) {
            System.err.println("ERROR: " + e.getMessage());
            System.exit(1);
        }
    }
    
    @Override
    public void onMessage(MessageEvent event) {
        User u = event.getUser();
        Channel c = event.getChannel();
        PircBotX bot = event.getBot();
        if(Yuki.isOpChannel(c)
           && !Yuki.isIgnored(u)
           && event.getMessage().startsWith("!vote")) {
            String[] args = event.getMessage().split(" ", 3);
            String subcmd = null;
            String parameters = null;
            
            if(args.length >= 2)
                subcmd = args[1];
            if(args.length >= 3)
                parameters = args[2];
            
            if(subcmd == null)
                return;
           
            try {
                if(subcmd.equals("list")) {
                    PreparedStatement s = conn.prepareStatement(
                            "SELECT id,title,closed FROM votes WHERE channel = ?");
                    s.setString(1, c.getName());
                    ResultSet rs = s.executeQuery();

                    bot.sendNotice(u, " id vote");
                    while(rs.next()) {
                        bot.sendNotice(
                            u, String.format("%3d %s%s", rs.getInt("id"),
                                             rs.getBoolean("closed") ? "!" : "",
                                             rs.getString("title")));
                    }
                    bot.sendNotice(u, "End of vote list");
                } else if(subcmd.equals("add")) {
                    if(parameters.startsWith("!")) {
                        bot.sendNotice(u, "The vote description may not begin"
                                + "with an exclamation mark.");
                        return;
                    }
                    PreparedStatement s = conn.prepareStatement(
                        "INSERT INTO votes(title,owner,channel,date) " +
                        "VALUES(?,?,?,?)");

                    String nick = getGroupNick(bot, u);
                    if(nick.equals("0"))
                        return;

                    int ts = (int)(System.currentTimeMillis()/1000L);
                    s.setString(1, parameters);
                    s.setString(2, nick);
                    s.setString(3, c.getName());
                    s.setInt(4, ts);
                    s.executeUpdate();
                    bot.sendNotice(u, "Added vote \"" + parameters + "\"");
                    
                    /* It should be impossible for two votes to be added at the
                     * same time. If it DOES happen, fuck everything.
                     * 
                     * We also can't check for the vote title, as JDBC probably
                     * has fucked around with some special characters on UPDATE.
                     */
                    s = conn.prepareStatement(
                            "SELECT id FROM votes WHERE channel = ?"
                            + "AND date = ?");
                    s.setString(1, c.getName());
                    s.setInt(2, ts);
                    ResultSet rs = s.executeQuery();
                    if(!rs.next()) {
                        bot.log("No result set after vote add?!");
                    }
                    bot.sendNotice(c, "Added vote id " + rs.getString("id")
                            + ": " + parameters);
                } else if(subcmd.equals("del")) {
                    PreparedStatement s = conn.prepareStatement(
                            "DELETE FROM votes WHERE id=?");
                    int id = 0;
                    try {
                        id = Integer.valueOf(parameters);
                    } catch(NumberFormatException e) {
                        bot.sendNotice(u,
                                "Invalid id " + parameters);
                        return;
                    }
                    s.setInt(1, id);
                    s.executeUpdate();
                    s = conn.prepareStatement(
                            "DELETE FROM vote_results WHERE voteid = ?");
                    s.setInt(1, id);
                    s.executeUpdate();
                    bot.sendNotice(u, "Deleted vote id " + id);
                } else if(subcmd.equals("info")) {
                    int id = 0;
                    try {
                        id = Integer.valueOf(parameters);
                    } catch(NumberFormatException e) {
                        bot.sendNotice(u, "Invalid id " + parameters);
                        return;
                    }

                    PreparedStatement s = conn.prepareStatement(
                            "SELECT id,title,owner,date,closed FROM votes " +
                            "WHERE channel = ? AND id = ?");
                    s.setString(1, c.getName());
                    s.setInt(2, id);
                    ResultSet rs = s.executeQuery();
                    if(rs.next()) {
                        bot.sendNotice(u, "Vote id " + rs.getInt("id") +
                                " for " + c.getName() + " added by " +
                                rs.getString("owner") + " on " + 
                                new SimpleDateFormat("MMM dd, yyyy").format(
                                    new Date((long)rs.getInt("date")*1000)) +
                                (rs.getBoolean("closed") ? " (Closed)" : ""));
                        
                        bot.sendNotice(u, rs.getString("title"));
                    } else {
                        bot.sendNotice(u, "Vote id not found");
                        return;
                    }

                    s = conn.prepareStatement("SELECT vote,voter FROM " +
                            "vote_results WHERE voteid = ?");
                    s.setInt(1, id);
                    rs = s.executeQuery();

                    int totalvotes = 0;
                    short yes = 0;
                    short no  = 0;
                    HashSet<String> yesvoters = new HashSet<String>();
                    HashSet<String> novoters = new HashSet<String>();

                    while(rs.next()) {
                        totalvotes++;
                        if(rs.getBoolean("vote")) {
                            yes++;
                            yesvoters.add(rs.getString("voter"));
                        } else {
                            no++;
                            novoters.add(rs.getString("voter"));
                        }
                    }

                    bot.sendNotice(u, "Total votes: " + totalvotes);
                    float yespercentage = (float)yes/((float)yes+(float)no);
                    if((yes+no) == 0)
                        yespercentage = (float)0;
                    StringBuilder out = new StringBuilder(
                            "In favor: " + yes + " [" + String.format("%.2f",
                            yespercentage*100) + "%] (");
                    Iterator i = yesvoters.iterator();
                    while(i.hasNext()) {
                        out.append(i.next());

                        // Don't append a comma on the last element!
                        if(i.hasNext())
                            out.append(", ");
                    }
                    out.append(")");
                    bot.sendNotice(u, out.toString());

                    float nopercentage = 1-yespercentage;
                    if(yespercentage == 0)
                        nopercentage = (float)0;
                    out = new StringBuilder(
                            "Against: " + no + " [" + String.format("%.2f",
                            (nopercentage*100)) + "%] (");
                    i = novoters.iterator();
                    while(i.hasNext()) {
                        out.append(i.next());
                        if(i.hasNext())
                            out.append(", ");
                    }
                    out.append(")");
                    bot.sendNotice(u, out.toString());
                } else if(subcmd.equals("do")) {
                    // !vote do 1 no
                    String[] splitparams = parameters.split(" ");
                    if(splitparams.length != 2) {
                        bot.sendNotice(u, "Not enough/too many parameters " +
                                "(Got " + splitparams.length + ", need 2)");
                        return;
                    }

                    int id = 0;
                    try {
                        id = Integer.valueOf(splitparams[0]);
                    } catch(NumberFormatException e) {
                        bot.sendNotice(u, "Invalid id " + parameters);
                        return;
                    }

                    PreparedStatement s = conn.prepareStatement(
                            "SELECT id,closed FROM votes WHERE id = ?");
                    s.setInt(1, id);
                    ResultSet rs = s.executeQuery();
                    if(!rs.next()) {
                        bot.sendNotice(u, "Vote id not found");
                        return;
                    } else {
                        if(rs.getBoolean("closed")) {
                            bot.sendNotice(u, "This vote is closed. You may not"
                                    + " change votes on it anymore.");
                            return;
                        }
                    }

                    s = conn.prepareStatement(
                            "SELECT voter FROM vote_results WHERE voteid = ?");
                    s.setInt(1, id);
                    rs = s.executeQuery();

                    String nick = getGroupNick(bot, u);
                    if(nick.equals("0"))
                        return;

                    while(rs.next()) {
                        if(rs.getString("voter").equals(nick)) {
                            /* We're changing votes. For simplicity's sake, just
                             * delete the old entry.
                             */
                            PreparedStatement ds = conn.prepareStatement(
                                    "DELETE FROM vote_results WHERE voteid = ? AND voter = ?");
                            ds.setInt(1, id);
                            ds.setString(2, nick);
                            ds.executeUpdate();
                        }
                    }

                    s = conn.prepareStatement("INSERT INTO vote_results (vote,voter,voteid) VALUES(?,?,?)");
                    String vote = splitparams[1];
                    int yes;
                    if(vote.equalsIgnoreCase("yes"))
                        yes = 1;
                    else if(vote.equalsIgnoreCase("no"))
                        yes = 0;
                    else {
                        bot.sendNotice(u, "That wasn't \"yes\" or \"no.\" " +
                                "Make up your mind!");
                        return;
                    }

                    s.setInt(1, yes);
                    s.setString(2, nick);
                    s.setInt(3, id);
                    s.executeUpdate();
                    bot.sendNotice(u, "You have voted \"" +
                            ((yes > 0) ? "yes" : "no") + ".\"");
                } else if(subcmd.equalsIgnoreCase("undo")) {
                    int id = 0;
                    try {
                        id = Integer.valueOf(parameters);
                    } catch(NumberFormatException e) {
                        bot.sendNotice(u, "Invalid id " + parameters);
                        return;
                    }

                    PreparedStatement s = conn.prepareStatement(
                            "SELECT id,closed FROM votes WHERE id = ?");
                    s.setInt(1, id);
                    ResultSet rs = s.executeQuery();
                    if(!rs.next()) {
                        bot.sendNotice(u, "Vote id not found.");
                        return;
                    } else {
                        if(rs.getBoolean("closed")) {
                            bot.sendNotice(u, "This vote is closed. You may not"
                                    + " change votes on it anymore.");
                            return;
                        }
                    }

                    String nick = getGroupNick(bot, u);
                    if(nick.equals("0"))
                        return;

                    s = conn.prepareStatement(
                            "DELETE FROM vote_results WHERE voteid = ? AND voter = ?");
                    s.setInt(1, id);
                    s.setString(2, nick);
                    s.executeUpdate();
                    bot.sendNotice(u, "You have deleted your vote.");
                } else if(subcmd.equals("open") || subcmd.equals("close")) {
                    int id = 0;
                    try {
                        id = Integer.valueOf(parameters);
                    } catch(NumberFormatException e) {
                        bot.sendNotice(u, "Invalid id " + parameters);
                        return;
                    }
                    
                    // If it's not close, it MUST be open
                    boolean doClose = subcmd.equals("close");

                    PreparedStatement s = conn.prepareStatement(
                            "SELECT id,closed FROM votes WHERE id = ?");
                    s.setInt(1, id);
                    ResultSet rs = s.executeQuery();
                    if(!rs.next()) {
                        bot.sendNotice(u, "Vote id not found.");
                        return;
                    }
                    
                    s = conn.prepareStatement("UPDATE votes SET closed = ? WHERE id = ?");
                    if(doClose)
                        s.setInt(1, 1);
                    else
                        s.setInt(1, 0);
                    
                    s.setInt(2, id);
                    s.executeUpdate();
                    bot.sendNotice(u, (doClose ? "Closed " : "Opened ") +
                            "vote id " + id + ".");
                }
            } catch(SQLException e) {
                bot.quitServer("Database error: " + e.getMessage());
                System.exit(1);
            }
        }
    }
    
    /**
     * Get the group nick of a given nick.
     * 
     * This doesn't abuse ChanServ's ACCESS command leak of group nick, but
     * relies on NickServ GNICK instead This is done again when called, even if
     * the nick has been looked up successfully before.
     * 
     * @param bot The bot object.
     * @param u The user object whose group nick should be found.
     * @return The group nick, "0" if none found or an error occurred.
     */
    private String getGroupNick(PircBotX bot, User u) {
        if(!Yuki.doLookup)
            return u.getNick();
        
        try {
            String nick = u.getNick();
            this.groupnick.put(nick, "0");
            bot.sendMessage("NickServ", "GNICK " + nick);

            long time = System.currentTimeMillis();
            synchronized(this.groupnick) {
                while (this.groupnick.get(nick).equals("0") &&
                        // Two seconds time to be slow
                        ((time + 2000) > System.currentTimeMillis())) {
                    this.groupnick.wait(1000);
                }
            }

            if(this.groupnick.get(nick).equals("0")) {
                bot.log("Couldn't find group nick for " + nick);
                return u.getNick();
            } else {
                return this.groupnick.remove(nick);
            }
        } catch(Exception e) {
            bot.log("GNICK ERROR: " + e.getMessage());
        }
        return "0";
    }   
    
    @Override
    public void onNotice(NoticeEvent event) {
        if(event.getChannel() != null)
            return;

        if(event.getUser().getNick().equals("NickServ")) {
            if(event.getMessage().startsWith("GNICK ")) {
                String[] msgsplit = event.getMessage().split(" ");
                String unick = msgsplit[1]; // User's nick
                String gnick = msgsplit[2]; // Group nick
                synchronized(this.groupnick) {
                    this.groupnick.put(unick, gnick);
                }
            }
        }
    }
    
    @Override
    public void onOp(OpEvent event) {
        PircBotX bot = event.getBot();
        if(Yuki.isIgnored(event.getRecipient()))
            return;

        try {
            PreparedStatement s = conn.prepareStatement(
                    "SELECT id,closed FROM votes WHERE channel = ?");
            s.setString(1, event.getChannel().getName());
            ResultSet rs = s.executeQuery();
            
            String nick = getGroupNick(bot, event.getRecipient());
            if(nick.equals("0"))
                return;
            
            // Set of ids the nick has to vote for
            HashSet<Integer> tovote = new HashSet<Integer>();
            
            while(rs.next()) { // iterating over ids
                if(rs.getBoolean("closed"))
                    continue;
                
                s = conn.prepareStatement(
                        "SELECT voter FROM vote_results WHERE voteid = ? AND voter = ?");
                s.setInt(1, rs.getInt("id"));
                s.setString(2, nick);
                ResultSet vrs = s.executeQuery();
                if(vrs.next())
                    continue;
                
                tovote.add(rs.getInt("id"));
            }
            
            StringBuilder buf = new StringBuilder("Please vote on vote(s): \002");
            if(tovote.isEmpty())
                return;
            
            Iterator i = tovote.iterator();
            while(i.hasNext()) {
                buf.append(i.next());
                if(i.hasNext())
                    buf.append("\002, \002");
            }
            buf.append("\002").append(" [").append(event.getChannel().getName()).append("].");
            
            bot.sendNotice(event.getRecipient(), buf.toString());
        } catch(SQLException e) {
            bot.quitServer("Database error: " + e.getMessage());
            System.exit(1);
        }
    }
}
