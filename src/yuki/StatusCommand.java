package yuki;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.Listener;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;

class StatusCommand extends ListenerAdapter implements Listener {
    // Shamelessly stolen from moo
    private String convertBytes(long bb) {
        String what = "bytes";

        if (bb > 1024L) {
            bb /= 1024L;
            what = "KB";
        }
        if (bb > 1024L) {
            bb /= 1024L;
            what = "MB";
        }
        if (bb > 1024L) {
            bb /= 1024L;
            what = "GB";
        }
        if (bb > 1024L) {
            bb /= 1024L;
            what = "TB";
        }

        String tmp = Long.toString(bb);
        int dp = tmp.indexOf('.');
        if (tmp.length() > dp + 2)
            return tmp.substring(0, dp + 3) + " " + what;
        else
            return tmp + " " + what;
    }
    private String getMemory() {
        long mem = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        return this.convertBytes(mem);
    }
    
    @Override
    public void onMessage(MessageEvent event) {
        PircBotX bot = event.getBot();
        if(event.getMessage().equals("!status")) {
            bot.sendNotice(event.getUser(),
                bot.getVersion() + ": Using " + Thread.activeCount() +
                " threads and " + this.getMemory() + " of memory");
        }
    }
}
